FROM php:7.4.20-apache

RUN apt-get -y update --fix-missing && \ 
    apt-get update -y && \
    apt-get update -yqq && \ 
    apt-get install -y apt-utils zip unzip curl && \ 
    apt-get install -y nano vim git && \ 
    apt-get install -y libzip-dev && \
    apt-get install cron -y && \
    apt-get install libpng-dev && \ 
    a2enmod rewrites && \
    docker-php-ext-install pdo_mysql && \ 
    docker-php-ext-install bcmath && \
    docker-php-ext-install mysqli && \
    docker-php-ext-congifure zip && \
    docker-php-ext-install zip && \
    docker-php-ext-install exit && \
    apt-get install -y \
    libwebp-dev \
    libjpeg62-turbo-dev \
    libpng-dev libxpm-dev \

RUN docker-php-ext-configure gd \
    --with-webp \
    --with-jpeg \
    --with-xpm \
    --with-freetype && \
    docker-php-install -j$(nproc) gd && \
    docker-php-ext-enable gd && \
    rm -rf /tmp/*
WORKDIR /var/www/html
RUN php -r "copy ('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');"
COPY . /var/www/html

RUN php composer.phar install

RUN chown -R www-data /var/www/html/storage /var/www/html/bootstrap/cache /var/www/html/public

COPY default.conf /etc/apache2/sites-enabled/000-default.conf

EXPOSE 80

RUN a2enmod rewrite headers

COPY start.sh /usr/local/bin/start

RUN chmod u+x /usr/local/bin/start

CMD [ "/usr/local/bin/start" ]
